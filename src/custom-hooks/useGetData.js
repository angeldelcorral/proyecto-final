import { useState } from "react"; 
import propTypes from "prop-types";
import { urlGetDataList, urlSearchList, urlGetImage } from "../constants";

const useGetData = (tipoData) => {
  const [datos, setDatos] = useState([]);
  const [offset, setOffset] = useState(0);
  const [showLoader, setShowLoader] = useState(false);
  const [totalDatos, setTotalDatos] = useState(0);
  const [disabledForwardBtn, setDisabledForwardBtn] = useState(false);
  const [disabledBackwardBtn, setDisabledBackwardBtn] = useState(true);
  const [pagNumber, setPagNumber] = useState(1);
  const [totalPags, setTotalPags] = useState(0);
  const [showPaginator, setShowPaginator] = useState(true);
  const [searchValue, setSearchValue] = useState("");

  const limit = 20;

  const getDataList = () => {
    setShowLoader(true);
    setShowPaginator(true);
    {
      offset === 0
        ? setDisabledBackwardBtn(true)
        : setDisabledBackwardBtn(false);
    }
    {
      offset >= totalDatos && totalDatos > 0
        ? setDisabledForwardBtn(true)
        : setDisabledForwardBtn(false);
    }

    const getDataUrl = urlGetDataList(tipoData, offset, limit);
    {
      tipoData === "characters"
        ? fetch(getDataUrl)
            .then((res) => res.json())
            .then((data) => {
              const result = data.data.results.map((item, key = item.id) => ({
                name: item.name,
                id: key,
                description: item.description,
                img_url: urlGetImage(
                  item.thumbnail.path,
                  "landscape_large",
                  item.thumbnail.extension
                ),
                offset: item.offset,
              }));
            //   console.log(data.data)
              setDatos(result);
              setTotalDatos(parseInt(data.data.total));
              setTotalPags(Math.ceil(parseInt(data.data.total) / limit));
              setShowLoader(false);
            })
            .catch((error) => console.log("[ERROR]", error))
        : fetch(getDataUrl)
            .then((res) => res.json())
            .then((data) => {
              const result = data.data.results.map((item, key = item.id) => ({
                name: item.title,
                id: key,
                type: item.type,
                img_url: urlGetImage(
                  item.thumbnail.path,
                  "landscape_large",
                  item.thumbnail.extension
                ),
                startYear: item.startYear,
                rating: item.rating,
                offset: item.offset,
              }));
              setDatos(result);
              setTotalDatos(parseInt(data.data.total));
              setTotalPags(Math.ceil(parseInt(data.data.total) / limit));
              setShowLoader(false);
            })
            .catch((error) => console.log("[ERROR]", error));
    }
  };

  const getDataSearch = (tipoData, searchValue) => {
    console.log("getDataSearch");
    let offset = 0;
    let limit = 100;
    setShowLoader(true);
    const getDataUrl = urlSearchList(tipoData, searchValue, offset, limit);
    fetch(getDataUrl)
      .then((res) => res.json())
      .then((data) => {
        const result = data.data.results.map((item, key = item.id) => ({
          name: item.name,
          id: key,
          description: item.description,
          img_url: urlGetImage(
            item.thumbnail.path,
            "landscape_large",
            item.thumbnail.extension
          ),
        }));
        setDatos(result);
        setShowPaginator(false);
        setShowLoader(false);
      })
      .catch((error) => console.log("[ERROR]", error));
  };

  const findCharacter = (
    e,
    q_character = document
      .querySelector("#q_character")
      .value.toLowerCase()
      .trim()
  ) => {
    if (e.type === "keypress" && e.key !== "Enter") return;
    const words = q_character.match(/\w+/g);
    q_character = words && words.join(" ");
    if (q_character && q_character !== searchValue) {
      getDataSearch(tipoData, q_character);
    }
  };

  return {
    showLoader,
    datos,
    setOffset,
    offset,
    limit,
    setPagNumber,
    pagNumber,
    totalPags,
    disabledForwardBtn,
    disabledBackwardBtn,
    showPaginator,
    setShowPaginator,
    getDataList,
    setDatos,
    setShowLoader,
    findCharacter,
    setSearchValue,
  };
};

//  DOCUMENTACIÓN PROP-TYPE
useGetData.propTypes = {
  showLoader: propTypes.bool,
  datos: propTypes.array,
  offset: propTypes.number,
  limit: propTypes.number,
  pagNumber: propTypes.number,
  totalPags: propTypes.number,
  disabledForwardBtn: propTypes.bool,
  disabledBackwardBtn: propTypes.bool,
  showPaginator: propTypes.bool,
  getDataList: propTypes.func,
  findCharacter: propTypes.func,
};

export default useGetData;
