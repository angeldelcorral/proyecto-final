const apiHash = "hash=a9f5ff548ebffc9f3e932271e7844058";
const apiKey = "apikey=e9397c7d59f8c5bce7c261624c193759";
const apiTS = 'ts=1';

const apiUrl = 'https://gateway.marvel.com/v1/public/'
// const apiUrl = "https://gateway.marvel.com:443/v1/public/";


export const urlGetDataList = (type, offset, limit) =>
  `${apiUrl}${type}?offset=${offset}&limit=${limit}&${apiTS}&${apiKey}&${apiHash}`;

export const urlSearchList = (type, searchValue, offset, limit) =>
  `${apiUrl}${type}?nameStartsWith=${searchValue}&offset=${offset}&limit=${limit}&${apiTS}&${apiKey}&${apiHash}`;

export const urlGetImage = (url, type, extension) => `${url}/${type}.${extension}`

// http://gateway.marvel.com/v1/public/characters?ts=1&apikey=e9397c7d59f8c5bce7c261624c193759&hash=a9f5ff548ebffc9f3e932271e7844058
// http://gateway.marvel.com/v1/public/characters?ts=1&apiKey=e9397c7d59f8c5bce7c261624c193759&hash=2653d19eb05e28a56ca686f58dc680ff