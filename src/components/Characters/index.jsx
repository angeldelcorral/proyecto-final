import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Button, Container } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { CharacterContext } from "../../contexts/CharactersContext";
import Loader from "../Common/Loading";
import Paginator from "../Common/Paginator";
import MarvelCharacters from "./Characters";
import SearchCharacter from "./SearchCharacter";

const Characters = () => {
  const {
    showLoader,
    datos,
    setOffset,
    offset,
    limit,
    setPagNumber,
    pagNumber,
    totalPags,
    disabledForwardBtn,
    disabledBackwardBtn,
    showPaginator,
    getDataList,
    findCharacter,
  } = useContext(CharacterContext);
  
  return (

    <>
      {showLoader ? (
        <Loader width={"200px"} />
      ) : (
        <>
          <div className="space-between">
            <SearchCharacter findCharacter={findCharacter}></SearchCharacter>
            {showPaginator ? (
              <Paginator
                setOffset={setOffset}
                setPagNumber={setPagNumber}
                offset={offset}
                limit={limit}
                pagNumber={pagNumber}
                totalPags={totalPags}
                disabledBackwardBtn={disabledBackwardBtn}
                disabledForwardBtn={disabledForwardBtn}
                datos={datos}
              ></Paginator>
            ) : (
              <Container className="paginator-h">
                <Link to="/">
                  <Button onClick={(e) => getDataList(e)}>
                    <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>{" "}
                    Home
                  </Button>
                </Link>
              </Container>
            )}
          </div>
          <div className="center-h">
            <MarvelCharacters datos={datos}></MarvelCharacters>
          </div>
        </>
      )}
    </>
  );
};

export default Characters;
