import React from "react";
import { Container, Row, Card } from "react-bootstrap";

const MarvelCharacters = ({ datos }) => {
  return (
    <>
      <Container className="paginartor-h">

        <Row xs={1} md={2} className="g-3">
          {datos.length
            ? datos.map((dato, key = dato.url) => (
              <Card style={{ width: "20rem" }} key={key}>
                  <Card.Img variant="top" src={dato.img_url} />
                  <Card.Body>
                    <Card.Title>Information:</Card.Title>
                    <span>
                      <strong>Name:</strong> {dato.name}
                    </span>
                    {dato.description !== "" ? (
                      <>
                        <br />
                        <span>
                          <strong>Desc:</strong> {dato.description}
                        </span>{" "}
                      </>
                    ) : null}
                  </Card.Body>
                </Card>
              ))
              : ""}
        </Row>
      </Container>
    </>
  );
};

export default MarvelCharacters;
