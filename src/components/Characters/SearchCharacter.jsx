import React from "react";
import { Container, Form, Button } from "react-bootstrap";

const SearchCharacter = ({ findCharacter }) => (
  <>
    <Container style={{ marginTop: "1em" }}>
      <Form className="d-flex">
        <input
          id="q_character"
          type="text"
          name="search"
          onKeyPress={(e) => findCharacter(e)}
          placeholder="Search Character"
        />
        <Button variant="outline-success" onClick={(e) => findCharacter(e)}>
          Search
        </Button>
      </Form>
    </Container>
  </>
);

export default SearchCharacter;
