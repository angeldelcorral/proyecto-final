import React from "react"
import { Link } from "react-router-dom";
import { Container, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

export default class ErrorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error) {
    // También puedes registrar el error en un servicio de reporte de errores
    // console.log('TENEMOS UN ERROR')
    this.setState({ hasError: true });
  }

  render() {
    // console.log(this.state.hasError)
    if (this.state.hasError) {
      // Puedes renderizar cualquier interfaz de repuesto
      return (
        <Container>
          <div className="error">
            <h1 className="error-text"> UPS!!! </h1>
            <h2 className="error-text">UH OH! Something went wrong.</h2>
            <div className="error-text">
              <Link to="/">
                <Button>
                  <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon> Back to home
                </Button>
              </Link>
            </div>
          </div>
        </Container>
      );
    }

    return this.props.children;
  }
}
