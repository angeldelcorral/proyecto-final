import React from "react";
import { Container, Row, Card } from "react-bootstrap";

const MarvelSeries = ({ datos }) => {
  return (
    <>
      <Container className="center-h">
        <Row xs={1} md={2} className="g-3">
          {datos.length
            ? datos.map((dato, key = dato.url) => (
                <Card style={{ width: "20rem" }} key={key}>
                  <Card.Img variant="top" src={dato.img_url} />
                  <Card.Body>
                    <Card.Title>Information:</Card.Title>
                    <span>
                      <strong>Name:</strong> {dato.name}
                    </span>
                    {dato.startYear !== "" ? (
                      <>
                        <br />
                        <span>
                          <strong>Start Year:</strong> {dato.startYear}
                        </span>{" "}
                      </>
                    ) : null}
                    {dato.type !== "" ? (
                      <>
                        <br />
                        <span>
                          <strong>Type:</strong> {dato.type}
                        </span>{" "}
                      </>
                    ) : null}
                    {dato.rating !== "" ? (
                      <>
                        <br />
                        <span>
                          <strong>Rating:</strong> {dato.rating}
                        </span>{" "}
                      </>
                    ) : null}
                  </Card.Body>
                </Card>
              ))
            : ""}
        </Row>
      </Container>
    </>
  );
};

export default MarvelSeries;
