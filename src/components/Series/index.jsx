import React, { useContext } from "react";
import { Container } from "react-bootstrap"
import { SerieContext } from "../../contexts/SeriesContext";
import Loader from "../Common/Loading";
import Paginator from "../Common/Paginator";
import MarvelSeries from "./Series";

const Series = () => {
  const {
    showLoader,
    datos,
    setOffset,
    offset,
    limit,
    setPagNumber,
    pagNumber,
    totalPags,
    disabledForwardBtn,
    disabledBackwardBtn,
  } = useContext(SerieContext);

  return (
    <>
      {showLoader ? (
        <Loader width={"200px"} />
      ) : (
        <>
          <div className="space-between">
            <Container style={{ marginTop: "1em" }}>{" "}</Container>
            <Paginator
              setOffset={setOffset}
              setPagNumber={setPagNumber}
              offset={offset}
              limit={limit}
              pagNumber={pagNumber}
              totalPags={totalPags}
              disabledBackwardBtn={disabledBackwardBtn}
              disabledForwardBtn={disabledForwardBtn}
              datos={datos}
            ></Paginator>
          </div>
          <div className="center-h">
            <MarvelSeries datos={datos}></MarvelSeries>
          </div>
        </>
      )}
    </>
  );
};

export default Series;
