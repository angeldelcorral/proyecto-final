import React from "react";
import { Link } from "react-router-dom";
import { Container, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const NotFound = () => {
  return (
    <Container>
      <div className="error">
        <h1 className="error-text"> 404 </h1>
        <h2 className="error-text">UH OH! You're lost.</h2>
        <p className="error-text">
          The page you are looking for, does not exist. How you got here is a
          mystery. <br />
          But you can click the button below to go back to the homepage.
        </p>
        <div className="error-text">
          <Link to="/">
            <Button>
              <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon> Home
            </Button>
          </Link>
        </div>
      </div>
    </Container>
  );
};

export default NotFound;
