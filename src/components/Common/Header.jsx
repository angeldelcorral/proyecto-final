import React from "react";
import {Link} from "react-router-dom"
import { Navbar, Nav, Container } from "react-bootstrap";
import LogoMarvel from "../../assets/images/Marvel_Logo.svg";

const Header = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark" expand="lg" sticky="top">
        <Container fluid>
          <Navbar.Brand>
            <img src={LogoMarvel} alt="Marvel" width="70px" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
                <Link className="nav-link" to="/">Characters</Link>
                <Link className="nav-link" to="/series">Series</Link>
            </Nav>
            <Nav>
              <Nav.Link
                href="https://www.linkedin.com/in/angel-del-corral/"
                target="_blanck"
              >
                Made by <strong> Angel Del Corral</strong>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default Header;
