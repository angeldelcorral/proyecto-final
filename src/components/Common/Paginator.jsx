import React from "react";
import propTypes from "prop-types";
import { Container, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBackward, faForward } from "@fortawesome/free-solid-svg-icons";

const Paginator = ({
  setOffset,
  setPagNumber,
  totalPags,
  offset,
  limit,
  pagNumber,
  disabledBackwardBtn,
  disabledForwardBtn,
  datos,
}) => {
  const handleForward = () => {
    let newValue = 0;
    if (offset < datos.offset) {
      newValue = datos.offset + limit;
    } else {
      newValue = offset + limit;
    }
    setOffset(newValue);
    setPagNumber(pagNumber + 1);
    // console.log(newValue);
  };

  const handleBackward = () => {
    let newValue = 0;
    if (offset < datos.offset) {
      newValue = datos.offset - limit;
    } else {
      newValue = offset - limit;
    }
    setOffset(newValue);
    setPagNumber(pagNumber - 1);
    // console.log(newValue);
  };

  return (
    <>
      <Container className="paginator-h">
        <div className="error-text">
          <Button onClick={handleBackward} disabled={disabledBackwardBtn}>
            <FontAwesomeIcon icon={faBackward} /> Previous
          </Button>
          <span className="paginator-h">
            &nbsp;&nbsp;Pag {pagNumber} / {totalPags}&nbsp;&nbsp;
          </span>
          <Button onClick={handleForward} disabled={disabledForwardBtn}>
            Next <FontAwesomeIcon icon={faForward} />
          </Button>
        </div>
      </Container>
    </>
  );
};

Paginator.propTypes = {
  setOffset: propTypes.func,
  setPagNumber: propTypes.func,
  totalPags: propTypes.number,
  offset: propTypes.number,
  limit: propTypes.number,
  pagNumber: propTypes.number,
  disabledBackwardBtn: propTypes.bool,
  disabledForwardBtn: propTypes.bool,
  datos: propTypes.array,
  newValue: propTypes.number,
};

export default Paginator;
