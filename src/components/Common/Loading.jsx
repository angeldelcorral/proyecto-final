import React from "react";
import propTypes from "prop-types";
import imgLoader from "../../assets/images/loading_infinite.gif";

const Loader = ({ width }) => {
  return (
    <div className="center-v">
      <div className="center-v">
        <img
          src={imgLoader}
          alt="Loading...."
          className="loader-img"
          width={width}
        />
      </div>
    </div>
  );
};

Loader.propTypes = {
  width: propTypes.string,
};

export default Loader;
