import React, { Suspense, lazy } from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import CharactersContextProvider from "./contexts/CharactersContext.jsx";
import Characters from "./components/Characters";
import Layout from "./components/Common/Layout";
import ErrorPage from "./components/ErrorComponent";
import Series from "./components/Series";
import Loader from "./components/Common/Loading";
import "./App.css";

const SeriesContextProvider = lazy(() =>
  import("./contexts/SeriesContext.jsx")
);

const NotFound = lazy(() => import("./components/NotFound"));

const App = () => (
  <BrowserRouter>
    <Layout>
      <Suspense
        fallback={
          <div>
            <Loader />
          </div>
        }
      >
        <Switch>
          <Route exact path="/series">
            <ErrorPage>
              <SeriesContextProvider>
                <Series />
              </SeriesContextProvider>
            </ErrorPage>
          </Route>
          <Route exact path="/">
            <ErrorPage>
              <CharactersContextProvider>
                <Characters />
              </CharactersContextProvider>
            </ErrorPage>
          </Route>
          <Route path="/404">
            <NotFound />
          </Route>
          <Redirect exact={false} to="/404" />
        </Switch>
      </Suspense>
    </Layout>
  </BrowserRouter>
);

export default App;
