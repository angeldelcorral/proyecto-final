import React, { createContext, useEffect } from "react";
import useGetData from "../custom-hooks/useGetData";

export const CharacterContext = createContext();

const CharactersContextProvider = ({ children }) => {
  const {
    showLoader,
    datos,
    setOffset,
    offset,
    limit,
    setPagNumber,
    pagNumber,
    totalPags,
    disabledForwardBtn,
    disabledBackwardBtn,
    showPaginator,
    setShowPaginator,
    getDataList,
    setDatos,
    setShowLoader,
    findCharacter,
  } = useGetData("characters");

  useEffect(() => {
    getDataList()
  }, [offset])
  


  return (
    <CharacterContext.Provider
      value={{
        showLoader,
        datos,
        setOffset,
        offset,
        limit,
        setPagNumber,
        pagNumber,
        totalPags,
        disabledForwardBtn,
        disabledBackwardBtn,
        showPaginator,
        setShowPaginator,
        getDataList,
        setDatos,
        setShowLoader,
        findCharacter,
      }}
    >
      {children}
    </CharacterContext.Provider>
  );
};

export default CharactersContextProvider;
