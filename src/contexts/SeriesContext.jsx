import React, { createContext, useEffect } from "react";
import useGetData from "../custom-hooks/useGetData";

export const SerieContext = createContext();

const SeriesContextProvider = ({ children }) => {
  const {
    showLoader,
    datos,
    setOffset,
    offset,
    limit,
    setPagNumber,
    pagNumber,
    totalPags,
    disabledForwardBtn,
    disabledBackwardBtn,
    getDataList,
  } = useGetData("series");

  useEffect(() => {
    getDataList();
  }, [offset]);

  return (
    <SerieContext.Provider
      value={{
        showLoader,
        datos,
        setOffset,
        offset,
        limit,
        setPagNumber,
        pagNumber,
        totalPags,
        disabledForwardBtn,
        disabledBackwardBtn,
      }}
    >
      {children}
    </SerieContext.Provider>
  );
};

export default SeriesContextProvider;
