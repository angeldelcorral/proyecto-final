# Proyecto final - Gen-8 React.js

# Estructura del proyecto

# 1 Conexión a la API Develop Marvel Portal
 Consumo de api mediante fetch

# 2 Rutas / navegacion / lazy loading
Implementado en archivo App.js

# 3 Patrón de arquitectura
El patrón implementado es de contexto / parcelas

## patrón de diseño
Provider

# 4 Aplicar Arquitectura limpia basada en patrones de arquitecturas vistos en el curso y buenas prácticas de Code Splitting
Se aplican Clean Arquitecturem webpack, babel, lazy loading, y error boundary, estu ultimo en un componente de nombre ErrorComponent

# 5 patron de diseño Prodiver

# 6 Implementacion de pag 404
Se implementa la pagina 404 con un boton para volvel al HOME

# 7 Aplicar el uso de hooks y ciclos de vida basado en componentes funcionales
Se aplican los hooks de useState, useEffect

# 8 Creación de algunos custom hook
Se crea un custom hook maestro, del que se alimenta toda la aplicación

# 9 Utilización de Prop-Types en por lo menos 3 componentes
Se prototipan 3 componentes:
- Custom Hook - useGetData
- Paginator
- Loading

# 10 No hay asignacion para el 10, jejeje

# 12 La UI la pueden implementar como mas les acomode y crear el responsive design
Se utilizó Bootstraps (react-bootstraps)
para los iconos Fontawesome Icons

# 13 Investigar y aplicar el HOC de ErrorBoundary (límites de error) a su proyecto que es parte de Code Splitting con la opción de recovery hacia la página principal de su proyecto
Se realizó una página con el patron de la pagina 404 pero con el mensaje UPS!!! Something went wrong y un boton que redirige al home

